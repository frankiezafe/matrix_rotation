shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx,unshaded;

// rotations in radian
uniform float rotation_x;
uniform float rotation_y;
uniform float rotation_z;

uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;


void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	// pre-processing all cosinus & sinus
	float cx = cos( rotation_x );
	float sx = sin( rotation_x );
	float cy = cos( rotation_y );
	float sy = sin( rotation_y );
	float cz = cos( rotation_z );
	float sz = sin( rotation_z );
	// building rotation matrices
	mat3 mx = mat3( vec3( 1.0,0.0,0.0 ), vec3( 0.0,cx,sx ), vec3( 0.0,-sx,cx ) );
	mat3 my = mat3( vec3( cy,0.0,-sy ), vec3( 0.0,1.0,0.0 ), vec3( sy,0.0,cy ) );
	mat3 mz = mat3( vec3( cz,sz,0.0 ), vec3( -sz,cz,0.0 ), vec3( 0.0,0.0,1.0 ) );
	// applying matrices in YXZ order on vertex
	VERTEX = my * mx * mz * VERTEX;
}




void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
}
