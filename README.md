# Matrix rotation

![screenshot of project loaded in godot](https://polymorph.cool/wp-content/uploads/2019/03/Screenshot-from-2019-03-12-01-06-11.png)

This project explains how to build a rotation matrix out of 3 angles (eulers).

The method can be applied anywhere as long as you follow the rotation order of your context / engine.

video: https://peertube.mastodon.host/videos/watch/b7de1c33-a23f-463c-a926-43527cd82071

## how to use

Open *matrix_rotation.tscn*, select *root* in scene panel and modify the rotation axis.

## the code

The gdscript version is in *matrix_rotation.gd*, in the process function. It also updates the rotation of the shader, see line 143

A shader called *matrix_rotation.shader* shows how to do the same process in the vertex shader.

## the approach

A rotation matrix is composed of 3 vector 3d, each one containing the coordinates of the X, Y & Z axis rotated.

Therefore, a identity matrix (having no rotation) contains these values:

```
X | Y | Z
-----
1 | 0 | 0
0 | 1 | 0
0 | 0 | 1 
```

Reverse is also true: if you manage to compute these 3 axis and push them in a matrix, the matrix will express a rotation.

This what we do in the script. We generate one matrix per axis of rotation representing the orientation of:

- Y & Z axis under a X rotation, represented by cosinus and sinus of the angle
- X & Z axis under a Y rotation, idem
- X & Y axis under a Z rotation, idem

As the math to compute it at once are a bit complex, we just apply them on position in the conventional YXZ order used in godot.

```
var p = MATRIX_Y * MATRIX_X * MATRIX_Z * position
```

For newbies, multiplication of matrix are not commutative, meaning the order is crucial to get the right rotation.

The method might seems complex, but it has the advantage of working in any context.

## resource

- https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
- https://docs.godotengine.org/en/3.0/classes/class_basis.html?highlight=Basis (the 3x3 matrix in godot)
