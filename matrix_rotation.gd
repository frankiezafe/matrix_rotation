tool

extends Spatial

export (int, "X","Y","Z","ALL (3 matrices)","ALL (1 matrix)") var rotation_axis = 0 setget _rotation_axis
export (float, 0,5) var rotation_speed = 0.5

func _rotation_axis(i):
	rotation_axis = i
	a = 0

var c = null
var x = null
var y = null
var z = null
var shader = null

var a = 0

func prepare():
	c = get_node( "center" )
	x = get_node( "x" )
	y = get_node( "y" )
	z = get_node( "z" )
	shader = get_node( "shader" )

func _ready():
	prepare()

func _process(delta):
	
	if c == null:
		prepare()
	
	var radian = Vector3(0,0,0)
	var degrees = Vector3(0,0,0)
	
	match rotation_axis:
		
		0: # X axis
			radian.x = a
			degrees.x = a * 180 / PI
			x.translation = Vector3( 1,0,0 ) * 2
			y.translation = Vector3( 0, cos( a ), sin( a ) ) * 2
			z.translation = Vector3( 0, -sin( a ), cos( a ) ) * 2
		
		1: # Y axis
			radian.y = a
			degrees.y = a * 180 / PI
			x.translation = Vector3( cos( a ), 0, -sin( a ) ) * 2
			y.translation = Vector3( 0,1,0 ) * 2
			z.translation = Vector3( sin( a ), 0, cos( a ) ) * 2
		
		2: # Z axis
			radian.z = a
			degrees.z = a * 180 / PI
			x.translation = Vector3( cos( a ), sin( a ), 0 ) * 2
			y.translation = Vector3( -sin( a ), cos( a ), 0 ) * 2
			z.translation = Vector3( 0,0,1 ) * 2
		
		3: # ALL, let's mix everything!
			radian.x = a * 1.5
			radian.y = a * 0.5
			radian.z = a * 1.0
			degrees.x = radian.x * 180 / PI
			degrees.y = radian.y * 180 / PI
			degrees.z = radian.z * 180 / PI
			
			# same process as step 0 to 2, excepted we pre-process
			# cosinus & sinus to save a bit of process (usefull in shader)
			var cx = cos( radian.x )
			var sx = sin( radian.x )
			var cy = cos( radian.y )
			var sy = sin( radian.y )
			var cz = cos( radian.z )
			var sz = sin( radian.z )
			
			var mat3_x = Basis(
				Vector3( 1,0,0 ), 
				Vector3( 0, cx, sx ),
				Vector3( 0, -sx, cx ) 
			)
			var mat3_y = Basis(
				Vector3( cy, 0, -sy ), 
				Vector3( 0,1,0 ),
				Vector3( sy, 0, cy )
			)
			var mat3_z = Basis(
				Vector3( cz, sz, 0 ), 
				Vector3( -sz, cz, 0 ),
				Vector3( 0,0,1 )
			)
			
			# YXZ order !!!
			x.translation = mat3_y.xform( mat3_x.xform( mat3_z.xform( Vector3(1,0,0) ) ) ) * 2
			y.translation = mat3_y.xform( mat3_x.xform( mat3_z.xform( Vector3(0,1,0) ) ) ) * 2
			z.translation = mat3_y.xform( mat3_x.xform( mat3_z.xform( Vector3(0,0,1) ) ) ) * 2
		
		4: # ALL, faster
			radian.x = a * 1.5
			radian.y = a * 0.5
			radian.z = a * 1.0
			degrees.x = radian.x * 180 / PI
			degrees.y = radian.y * 180 / PI
			degrees.z = radian.z * 180 / PI
			
			# same process as step 0 to 2, excepted we pre-process
			# cosinus & sinus to save a bit of process (usefull in shader)
			var cx = cos( radian.x )
			var sx = sin( radian.x )
			var cy = cos( radian.y )
			var sy = sin( radian.y )
			var cz = cos( radian.z )
			var sz = sin( radian.z )
			
			# YXZ order !!!
			var mat3 = Basis(
				Vector3( cy, 0, -sy ), 
				Vector3( 0,1,0 ),
				Vector3( sy, 0, cy )
			) * Basis(
				Vector3( 1,0,0 ), 
				Vector3( 0, cx, sx ),
				Vector3( 0, -sx, cx ) 
			) * Basis(
				Vector3( cz, sz, 0 ), 
				Vector3( -sz, cz, 0 ),
				Vector3( 0,0,1 )
			)
			x.translation = mat3.xform( Vector3(1,0,0) ) * 2
			y.translation = mat3.xform( Vector3(0,1,0) ) * 2
			z.translation = mat3.xform( Vector3(0,0,1) ) * 2
	
	# increasing the angle of rotation
	a += rotation_speed * delta
	
	# applying rotation on center
	c.rotation_degrees = degrees
	
	# applying rotation via vertex shader
	shader.material_override.set_shader_param( "rotation_x", radian.x )
	shader.material_override.set_shader_param( "rotation_y", radian.y )
	shader.material_override.set_shader_param( "rotation_z", radian.z )